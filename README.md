# screenplay-cli

A script to create and a script to print your screenplay

## Screenplay creator "twrite"

This script give you a TUI to create screenplay scripts. If you use the print function it will create a HTML file in the same directory of the screenplay script. This you can open with your Browser and print to PDF in the correct format.

## How to use

Copy the twriter script into your $PATH direction.
Open a terminal in the target direction for your screenplay file.
Start the creator script with `twriter` or open existing file with `twriter <filename>`.
Then you create your screenplay script like this:

Use this letters at the beginning of every line followed by a SPACE and finished with ENTER.

 Use `Title: ` `Credit: ` `Author: ` `Source: ` `Notes: ` `Copyright: ` to create  the front page and `Dc: ` for the Description page.
	                                      
And for the content use `s ` Scene, `a ` Action, `c ` Character, `p ` Parentheticals,  `d ` Dialogue, `t ` Transition, `br ` Page brack `e ` Edit, `print` create HTML file, Ctrl+c Quit

Example script:

```
Title: Big Fish
Credit: written by
Author: John August
Source: based on the novel by Daniel Wallace
Notes: FINAL PRODUCTION DRAFT
Notes: includes post-production dialogue
Notes: and omitted scenes
Copyright: (c) 2003 Columbia Pictures
Dc: This is a Southern story, full of lies and fabrications, but truer for their inclusion.
s outside
a It's winter and coold.
t move left
s inside
a Tow guys are busy in a messi kitchen.
c thomas
p out of brave
d It's time to go. Let's make this dialogue much longer so we have a better example.
c karl
p in rush
d let's move
t move right
s in the drive way
a A old Honda with snow on the top and icy windows.
br 
c thomas
d Get in the fucking car.
t cut
```
