#!/bin/bash
#T-Writer - CLI Screenplay writer.
#by Thomas Pautler 2022
#  ______      ____
# /_  __/___  / __ \____ _      __
#  / / / __ \/ /_/ / __ \ | /| / /
# / / / /_/ / ____/ /_/ / |/ |/ /
#/_/  \____/_/    \____/|__/|__/
#
print_cc() {
     [[ $# == 0 ]] && return 1

	echo $1 | fmt -g 50 | while read line; do

     declare -i TERM_COLS="$(tput cols)"
     declare -i str_len="${#line}"
     [[ $str_len -ge $TERM_COLS ]] && {
          echo "$line";
          return 0;
     }

     declare -i filler_len="$(( (TERM_COLS - str_len) / 2 -6 ))"
     [[ $# -ge 2 ]] && ch="${2:0:1}" || ch=" "
     filler=""
     for (( i = 0; i < filler_len; i++ )); do
          filler="${filler}${ch}"
     done

     printf "\t$4%s$RS$3%s$RS$4%s$RS" "$filler" "$line" "$filler"
     [[ $(( (TERM_COLS - str_len) % 2 )) -ne 0 ]] && printf "$4%s$RS" "${ch}"
     printf "\n"

	done
	 return 0
}

print_c() {
     [[ $# == 0 ]] && return 1

     declare -i TERM_COLS="$(tput cols)"
     declare -i str_len="${#1}"
     [[ $str_len -ge $TERM_COLS ]] && {
          echo "$1";
          return 0;
     }

     declare -i filler_len="$(( (TERM_COLS - str_len) / 2 -6 ))"
     [[ $# -ge 2 ]] && ch="${2:0:1}" || ch=" "
     filler=""
     for (( i = 0; i < filler_len; i++ )); do
          filler="${filler}${ch}"
     done

     printf "\t$4%s$RS$3%s$RS$4%s$RS" "$filler" "$1" "$filler"
     [[ $(( (TERM_COLS - str_len) % 2 )) -ne 0 ]] && printf "$4%s$RS" "${ch}"
     printf "\n"

     return 0
}

print_r() {
     [[ $# == 0 ]] && return 1

     declare -i TERM_COLS="$(tput cols)"
     declare -i str_len="${#1}"
     [[ $str_len -ge $TERM_COLS ]] && {
          echo "$1";
          return 0;
     }

     declare -i filler_len="$(( (TERM_COLS - str_len ) / 2 -6 ))"
     [[ $# -ge 2 ]] && ch="${2:0:1}" || ch=" "
     filler=""
     for (( i = 0; i < filler_len; i++ )); do
          filler="${filler}${ch}"
     done

     printf "\t$4%s%s$RS$3%s$RS" "$filler" "$filler" "$1"
     [[ $(( (TERM_COLS - str_len) % 2 )) -ne 0 ]] && printf "$4%s$RS" "${ch}"
     printf "\n"

     return 0
}

print_l() {
     [[ $# == 0 ]] && return 1

     declare -i TERM_COLS="$(tput cols)"
     declare -i str_len="${#1}"
     [[ $str_len -ge $TERM_COLS ]] && {
          echo "$1";
          return 0;
     }

     declare -i filler_len="$(( (TERM_COLS - str_len) / 2 -6 ))"
     [[ $# -ge 2 ]] && ch="${2:0:1}" || ch=" "
     filler=""
     for (( i = 0; i < filler_len; i++ )); do
          filler="${filler}${ch}"
     done

     printf "\t$3%s$RS$4%s%s$RS" "$1" "$filler" "$filler"
     [[ $(( (TERM_COLS - str_len) % 2 )) -ne 0 ]] && printf "$4%s$RS" "${ch}"
     printf "\n"

     return 0
} 

print_mlbc() {
    local longest=0
    local string_array=("${@}")
    for i in "${string_array[@]}"; do
      if [[ "${#i}" -gt "${longest}" ]]; then
        local longest=${#i}
        local longest_line="${i}" # Longest line
      fi
    done
  
    local edge=$(echo "$longest_line" | sed 's/./🭹/g' | sed 's/^🭰/🭹🭹🭹/' | sed 's/$/🭹🭹🭹🭹/')
    local middle_edge=$(echo "$longest_line" | sed 's/./\ /g' | sed 's/^\ /🭰\  /' | sed 's/\ $/\ \ 🭵/')
  
    print_c "${edge}"
    print_c "${middle_edge}"
  
    for i in "${string_array[@]}"; do
      local length_i=${#i}
      local length_ll="${#longest_line}"
     short_l=`if [[ "${length_i}" -lt "${length_ll}"  ]]; then
              printf "🭰 "
              local remaining_spaces=$((length_ll-length_l))
              printf "${i}"
              while [[ ${remaining_spaces} -gt ${#i} ]]; do
                      printf " "
                      local remaining_spaces=$((remaining_spaces-1))
              done
              printf " 🭵\n"
      else
        echo -e "🭰 ${i} 🭵"
      fi`
      print_c "$short_l"
    done
     
    print_c "${middle_edge}"
    print_c "${edge}"
}

dialogue() {
	echo -e "\t\t$1" | fmt -g 70
}

slogo() {
	print_l "━━━ T-SCREENPLAY " "━"
}

line() {
	print_c "━" "━"
}

help() {
	print_c "Use <Title: > <Credit: > <Author: > <Source: > <Notes: > <Copyright: > "
	print_c "for the front page and <Dc: > for the Description page."
	print_c "------"
	print_c "<s > Scene | <a > Action | <c > Character | <p > Parentheticals | <d > Dialogue"
	print_c "<t > Transition | <br > Page brack"
	 	 print_c "<e > Edit | <print> create HTML file | Ctrl+c Quit"
}

spp() {
	PRINT=$(echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">
	<html>
	<head>
		<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"/>
		<title>$1</title>
		<style type=\"text/css\">
			html { background:#191919; font-family: \"Courier Prime\", monospace;}
			body { background:#fff; width: 210mm; margin: 40px auto; }
			p { margin: 0.2em;}
			.content { padding: 25.4mm; padding-left: 38.1mm}
			.trans { text-align: right;}
			.character, .parent, .title, .credit, .author{ text-align: center;}
			.character { margin-top: 2em; }
			.title { margin-top: 50mm; }
			.author { font-weight: 700; margin-bottom: 3em;}
			.source { margin-bottom: 20em; }
			.dialog { padding-left: 25.4mm; width: 40ch; }
			.copyright { margin-top: 3em; }
			@media print {
				.br {page-break-after: always;}
				hr { display:none; }
			}
		</style>
	</head>
	<body lang=\"en-US\"><div class=\"content\">"
	cat "$1" | while read TEXT; do
	FUNC=$(echo $TEXT | cut -d " " -f1)
	case $FUNC in
		Title: | title:)
			RT=$(echo $TEXT | sed 's/^.*: //' | tr [a-z] [A-Z])
			echo "<h1 class=\"title\">$RT</h1>" ;;
		Credit: | credit:)
			RT=$(echo $TEXT | sed 's/^.*: //')
			echo "<p class=\"credit\">$RT</p>" ;;
		Author: | author:)
			RT=$(echo $TEXT | sed 's/^.*: //')
			echo "<p class=\"author\">$RT</p>" ;;
		Source: | source:)
			RT=$(echo $TEXT | sed 's/^.*: //')
			echo "<p class=\"dialog source\">$RT</p>" ;;
		Notes: | notes:)
			RT=$(echo $TEXT | sed 's/^.*: //')
			echo "<p class=\"trans\">$RT</p>" ;;
		Copyright: | copyright:)
			RT=$(echo $TEXT | sed 's/^.*: //')
			echo "<p class=\"action copyright br\">©️$RT</p><br><hr>" ;;
		Dc: | dc:)
			RT=$(echo $TEXT | sed 's/^.*: //')
			echo "<p class=\"dialog br\">$RT</p><br><hr>" ;;
		s | S)
			RT=$(echo $TEXT | sed 's/^. //' | tr [a-z] [A-Z])
			echo "<p class=\"scene\"><b>$RT</b></p>" ;;
		a | A)
			RT=$(echo $TEXT | sed 's/^. //')
			echo "<p class=\"action\">$RT</p>" ;;
		c | C)
			RT=$(echo $TEXT | sed 's/^. //' | tr [a-z] [A-Z])
			echo "<p class=\"character\">$RT</p>" ;;
		p | P)
			RT=$(echo $TEXT | sed 's/^. //')
			echo "<p class=\"parent\">($RT)</p>" ;;
		d |D)
			RT=$(echo $TEXT | sed 's/^. //')
			echo "<p class=\"dialog\">$RT</p>" ;;
		t | T)
			RT=$(echo $TEXT | sed 's/^. //' | tr [a-z] [A-Z])
			echo "<p class=\"trans\">$RT</p>" ;;
		br)
			echo "<div class=\"br\"><hr></div>" ;;
	esac
	done
	echo "</div>
	</body>
	</html>")
	echo $PRINT > "$NAME.html"
	xdg-open "$NAME.html"
	return
}

main() {
	echo -e "\e[7m"
	cat "$NAME" | while read TEXT; do
	FUNC=$(echo $TEXT | cut -d " " -f1)
	case $FUNC in
		Title: | title:)
			RT=$(echo $TEXT | sed 's/^.*: //' | tr [a-z] [A-Z])
			print_l " " ; print_c "$RT" ; print_l " " ; print_l " " ;;
		Credit: | credit:)
			RT=$(echo $TEXT | sed 's/^.*: //')
			print_c "$RT" ; print_l " " ;;
		Author: | author:)
			RT=$(echo $TEXT | sed 's/^.*: //')
			print_c "$RT" ; print_l " " ; print_l " " ;;
		Source: | source:)
			RT=$(echo $TEXT | sed 's/^.*: //')
			print_c "$RT" ; print_l " " ; print_l " " ; print_l " " ; print_l " " ;;
		Notes: | notes:)
			RT=$(echo $TEXT | sed 's/^.*: //')
			print_r "$RT " ;;
		Copyright: | copyright:)
			RT=$(echo $TEXT | sed 's/^.*: //')
			print_l " " ; print_l "  ©️$RT" ; print_l " " ; print_c "_" "_" ; print_l " ";;
		Dc: | dc:)
			RT=$(echo $TEXT | sed 's/^.*: //')
			print_l " " ; print_cc "$RT" ; print_l " " ; print_c "_" "_" ; print_l " ";;
		s | S)
			RT=$(echo $TEXT | sed 's/^. //' | tr [a-z] [A-Z])
			print_l "  $RT" ; print_l " " ;;
		a | A)
			RT=$(echo $TEXT | sed 's/^. //')
			print_l "  $RT" ; print_l " " ;;
		c | C)
			RT=$(echo $TEXT | sed 's/^. //' | tr [a-z] [A-Z])
			print_c "$RT" ;;
		p | P)
			RT=$(echo $TEXT | sed 's/^. //')
			print_c "($RT)" ;;
		d |D)
			RT=$(echo $TEXT | sed 's/^. //')
			print_cc "$RT" ; print_l " " ;;
		t | T)
			RT=$(echo $TEXT | sed 's/^. //' | tr [a-z] [A-Z])
			print_r "$RT " 
			print_l " " ;;
		br)
			print_l " " ; print_c "_" "_" ; print_l " ";;
	esac
	done
	echo -e "\e[27m"	
}

edit() {
	$EDITOR "$NAME"
}

slogo 
[[ -z "$1" ]] && read -p "Please enter file name: " NAME || NAME="$*"
touch "$NAME"

while true
do
clear
slogo
main
line ; help ; line
read -p "Enter: " TEXT
CHECK=$(echo $TEXT | cut -d " " -f1)
case $TEXT in
	print)
		spp "$NAME" ;;
	e)
		edit ;;
	*)
		echo -e "$TEXT" >> "$NAME"
esac

done
